#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2012 by Łukasz Langa
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from django.views.decorators.cache import never_cache
from dj.choices import Language
from lck.django.common import render, redirect, remote_addr, get_langs
from lck.django.tags.models import TagStem

from langapl.publisher.forms import PrivateKeyForm
from langapl.publisher.models import Entry, Content, PrivateNote


def recent(request):
    page = 'recent'
    for lang in get_langs(request):
        contents = Content.objects.filter(**lang).filter(
            entry__include_on_frontpage=True).select_related()[:10]
        if len(contents):
            break
    return render(request, 'recent.html', locals(), compute_etag=False)


def login(request):
    page = 'login'
    pass


def logout(request):
    page = 'logout'
    pass


def archive(request):
    page = 'archive'
    for lang in get_langs(request):
        contents = Content.objects.filter(**lang).filter(
            entry__include_on_frontpage=True).select_related()
        if len(contents):
            break
    return render(request, 'archive.html', locals(), compute_etag=False)


def show(request, entry_id, slug='?'):
    page = 'show'
    if request.user.is_superuser:
        queryset = Entry.admin_objects
    else:
        queryset = Entry.objects
    entry = get_object_or_404(queryset, pk=int(entry_id))
    bump_id = remote_addr(request)
    for lang in get_langs(request, {'slug': slug}):
        try:
            content = entry.content_set.get(**lang)
        except Content.DoesNotExist:
            continue
        else:
            break
    else:
        raise NotImplementedError()
    if content.slug != slug:
        return redirect(request, reverse('show', args=[entry_id,
            content.slug]))
    entry.bump(bump_id)
    content.bump(bump_id)
    tags = content.get_tags()
    if tags.count():
        similar_entries = []
        _s = 0
        for similar in content.similar(6):
            if _s == 6:
                break
            if content != similar[0]:
                similar_entries.append(similar)
                _s += 1
    try:
        prev_entry = Content.objects.filter(entry___sort_date__lt=
            entry._sort_date, language=content.language).order_by(
                '-entry___sort_date')[0]
    except (Content.DoesNotExist, IndexError):
        pass
    try:
        next_entry = Content.objects.filter(entry___sort_date__gt=
            entry._sort_date, language=content.language).order_by(
                'entry___sort_date')[0]
    except (Content.DoesNotExist, IndexError):
        pass
    return render(request, 'show.html', locals(), compute_etag=False)


@never_cache
def private(request, slug):
    page = 'private'
    request._cache_update_cache = False
    if request.user.is_superuser:
        queryset = PrivateNote.admin_objects
    else:
        queryset = PrivateNote.objects
    content = get_object_or_404(queryset, slug=slug)
    session_key = 'pn{}'.format(content.id)
    form = None
    if request.method == 'POST':
        form = PrivateKeyForm(data=request.POST, valid_key=content.private_key)
        if form.is_valid():
            request.session[session_key] = content.title
        else:
            content.bump(remote_addr(request),
                    failed_key=request.POST.get('key', '----'))
    if not request.user.is_superuser and \
            request.session.get(session_key) != content.title:
        if not form:
            form = PrivateKeyForm(valid_key=content.private_key)
        return render(
            request, 'private_auth.html', locals(), compute_etag=False,
        )
    content.bump(remote_addr(request))
    return render(request, 'show.html', locals(), compute_etag=False)


def tag(request, tag):
    page = 'tag'
    stem = get_object_or_404(TagStem, name=tag, language=Language.id_from_name(
        request.LANGUAGE_CODE))
    contents = TagStem.objects.get_content_objects(model=Content, stem=stem)
    return render(request, 'recent.html', locals(), compute_etag=False)


def tagcloud(request):
    page = 'tagcloud'
    pass
