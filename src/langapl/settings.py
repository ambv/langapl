#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Django settings for langapl."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
# from __future__ import unicode_literals #FIXME: Still broken as of Django 1.3.

from lck.django import current_dir_support
execfile(current_dir_support)

from lck.django import namespace_package_support
execfile(namespace_package_support)

#
# common stuff for each install
#

ADMINS = (
    ('Lukasz Langa', 'lukasz@langa.pl'),
)
MANAGERS = ADMINS
DEFAULT_FROM_EMAIL = 'lukasz@langa.pl'
SERVER_EMAIL = DEFAULT_FROM_EMAIL
TIME_ZONE = 'Europe/Warsaw'
LANGUAGE_CODE = 'en-gb'
from dj.choices import Language
LANGUAGES = Language(item=lambda choice: (choice.name.replace('_', '-'),
    choice.raw), filter=('en_gb', 'en_us', 'en', 'pl'))
SITE_ID = 1
USE_I18N = True
USE_L10N = True
USE_ETAGS = True
MEDIA_ROOT = CURRENT_DIR + 'uploads'
MEDIA_URL = '/uploads/'
STATIC_ROOT = CURRENT_DIR + 'static'
STATIC_URL = '/static/'
STATICFILES_DIRS = (
    CURRENT_DIR + 'media',
)
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'lck.django.staticfiles.LegacyAppDirectoriesFinder',
)
FILE_UPLOAD_TEMP_DIR = CURRENT_DIR + 'uploads-part'
TEMPLATE_LOADERS = (
    ('django.template.loaders.cached.Loader', (
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
    )),
)
MIDDLEWARE_CLASSES = (
#    'django.middleware.cache.UpdateCacheMiddleware',
    'django.middleware.gzip.GZipMiddleware',
    'lck.django.common.middleware.TimingMiddleware',
    'django.middleware.http.ConditionalGetMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'lck.django.common.middleware.SessionAwareLanguageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.redirects.middleware.RedirectFallbackMiddleware',
    'lck.django.activitylog.middleware.ActivityMiddleware',
    'lck.django.common.middleware.AdminForceLanguageCodeMiddleware',
#    'django.middleware.cache.FetchFromCacheMiddleware',
    'lck.django.flatpages.middleware.FlatpageFallbackMiddleware',
)
SESSION_ENGINE = 'django.contrib.sessions.backends.cache'
ROOT_URLCONF = 'langapl.urls'
TEMPLATE_DIRS = (CURRENT_DIR + "templates",)
LOCALE_PATHS = (CURRENT_DIR + "locale",)
INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.markup',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.comments',
    'django.contrib.redirects',
    'django.contrib.sitemaps',
    'django_crystal_small',
    'django_rq',
    'lck.django.activitylog',
    'lck.django.common',
    'lck.django.flatpages',
    'lck.django.profile',
    'lck.django.score',
    'lck.django.tags',
    'gunicorn',
    'south',
    'langapl.publisher',
)
TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.contrib.messages.context_processors.messages',
    'langapl.publisher.context_processors.defaults',
)
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'file': {
            'level': 'INFO',
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'when': 'midnight',
            'filename': None, # to be configured in settings-local.py
            'formatter': 'verbose',
            'interval': 1,
        },
        'console':{
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
        }
    },
    'formatters': {
        'verbose': {
            'datefmt': '%H:%M:%S',
            'format': '%(asctime)08s,%(msecs)03d %(levelname)-7s [%(processName)s %(process)d] %(module)s - %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'langapl': {
            'handlers': ['file'],
            'propagate': True,
            'level': 'INFO',
        },
    },
}
FORCE_SCRIPT_NAME = ''
ALLOWED_HOSTS = ('.lukasz.langa.pl', 'localhost')
AUTH_PROFILE_MODULE = 'publisher.Profile'
EDITOR_TRACKABLE_MODEL = AUTH_PROFILE_MODULE
TAG_AUTHOR_MODEL = AUTH_PROFILE_MODULE
# lck.django.activitylog settings
ACTIVITYLOG_PROFILE_MODEL = AUTH_PROFILE_MODULE
ACTIVITYLOG_MODE = 'rq'
ACTIVITYLOG_TASK_EXPIRATION = 30
CURRENTLY_ONLINE_INTERVAL = 120
BACKLINK_LOCAL_SITES = 'current'
# other
LOGIN_REDIRECT_URL = '/'
LOGIN_URL = '/login/'
LOGOUT_URL = '/logout/'
# django.contrib.messages settings
MESSAGE_STORAGE = 'django.contrib.messages.storage.session.SessionStorage'
# django.contrib.markup
RESTRUCTUREDTEXT_FILTER_SETTINGS = dict(
    initial_header_level = 2,
    #no_doc_title = True,
)

#
# stuff that should be customized in settings_local.py
#
SECRET_KEY = None
DEBUG = False
TEMPLATE_DEBUG = DEBUG
DUMMY_SEND_MAIL = DEBUG
SEND_BROKEN_LINK_EMAILS = DEBUG
DATABASES = dict(
    default = {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': CURRENT_DIR + 'development.db',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
        'OPTIONS': {'timeout': 30},
    },
)
CACHES = dict(
    default = dict(
        BACKEND = 'redis_cache.cache.RedisCache',
        LOCATION = 'localhost:6379:1',
        OPTIONS = dict(
            CLIENT_CLASS = 'redis_cache.client.DefaultClient',
            PARSER_CLASS = 'redis.connection.HiredisParser',
            PICKLE_VERSION = 2,
        ),
        KEY_PREFIX = 'LANGAPL',
    )
)
CACHE_MIDDLEWARE_ANONYMOUS_ONLY = False
CACHE_MIDDLEWARE_KEY_PREFIX = 'LANGAPL'
CACHE_MIDDLEWARE_SECONDS = 900
RQ_QUEUES = {
    'default': {
        'HOST': 'localhost',
        'PORT': 6379,
        'DB': 2,
    },
    'activitylog': {
        'HOST': 'localhost',
        'PORT': 6379,
        'DB': 2,
    },
}
LOGGING['handlers']['file']['filename'] = CURRENT_DIR + 'runtime.log'
DEFAULT_SAVE_PRIORITY = 0

#
# programmatic stuff that needs to be in
#

from lck.django import profile_support
execfile(profile_support)
