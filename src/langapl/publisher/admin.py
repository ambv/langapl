#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2012 Łukasz Langa

from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

from django.conf import settings
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User, Group
from django.template import Context, Template
from django.utils.translation import ugettext_lazy as _
from lck.django.common.admin import ModelAdmin,\
    ForeignKeyAutocompleteStackedInline, ForeignKeyAutocompleteTabularInline

from langapl.publisher.models import Entry, Content, PrivateNote,\
    PrivateAuthLog, Profile


class ContentInline(ForeignKeyAutocompleteStackedInline):
    model = Content
    prepopulated_fields = {'slug': ('title',)}
    extra = 2
    max_num = 2
    readonly_fields = ('created', 'modified', 'created_by', 'modified_by')
    fieldsets = (
        ("", {'fields': ['language',
                         ('title', 'slug'),
                         '_default_tags',
                         'summary_raw',
                         'content_raw'],
              'description': '', 'classes': []}),
    )

    def save_model(self, request, obj, form, change):
        if hasattr(obj, 'pre_save_model'):
            obj.pre_save_model(request, obj, form, change)
        obj.save()


class EntryAdmin(ModelAdmin):
    def item_url(self, arg):
        return '<a href="{}"><img alt="{}" src="{}lckd/icons/page_white_magnify'\
            '.png"></a>'.format(arg.get_absolute_url(),
                unicode(_("Show on site")), settings.STATIC_URL)
    item_url.allow_tags = True
    item_url.short_description = ''
    def title(self, arg):
        result = '<br>'.join(content.title for content in arg.content_set.all())
        if result:
            return result
        return _('empty entry')
    title.allow_tags = True
    title.short_description = _('title')
    list_display = ('item_url', 'title', 'publication_status', 'display_count',
            'created', 'modified', 'visible_from', 'visible_to')
    list_display_links = ('title',)
    search_fields = ('content__title', 'content__summary_raw')
    list_filter = ('publication_status', 'visible_from', 'visible_to')
    list_editable = ('publication_status',)
    inlines = [ContentInline]
    save_on_top = True
    fieldsets = (
        ("", {'fields': [('publication_status', 'include_on_frontpage'),
                          'comment_status',
                         ('visible_from', 'visible_to'),
                         ('created', 'modified')],
              'description': '', 'classes': []}),
    )

admin.site.register(Entry, EntryAdmin)


class ContentAdmin(ModelAdmin):
    def item_url(self, arg):
        return '<a href="{}"><img alt="{}" src="{}lckd/icons/page_white_magnify'\
            '.png"></a>'.format(arg.get_absolute_url(),
                unicode(_("Show on site")), settings.STATIC_URL)
    item_url.allow_tags = True
    item_url.short_description = ''
    list_display = ('item_url', 'title', 'get_publication_status_display',
            'display_count', 'created', 'modified', 'visible_from', 'visible_to')
    list_display_links = ('title',)
    search_fields = ('title', 'summary_raw')
    list_filter = ('entry__publication_status', 'entry__visible_from',
        'entry__visible_to')
    save_on_top = True

admin.site.register(Content, ContentAdmin)


class PrivateAuthLogInline(admin.TabularInline):
    model = PrivateAuthLog
    extra = 0
    exclude = ('modified',)
    readonly_fields = ('success', 'created', 'address', 'failed_key')


class PrivateNoteAdmin(ModelAdmin):
    inlines = [PrivateAuthLogInline]
    def item_url(self, arg):
        return '<a href="{}"><img alt="{}" src="{}lckd/icons/page_white_magnify'\
            '.png"></a>'.format(arg.get_absolute_url(),
                unicode(_("Show on site")), settings.STATIC_URL)
    item_url.allow_tags = True
    item_url.short_description = ''
    list_display = ('item_url', 'title', 'get_publication_status_display',
            'display_count', 'created', 'modified', 'visible_from', 'visible_to')
    list_display_links = ('title',)
    list_filter = ('publication_status', 'visible_from', 'visible_to')
    save_on_top = True
    search_fields = ('title', 'summary_raw')

admin.site.register(PrivateNote, PrivateNoteAdmin)


class ProfileInline(admin.StackedInline):
    model = Profile
    readonly_fields = ('last_active',)
    max_num = 1
    can_delete = False


class ProfileAdmin(UserAdmin):
    def groups_show(self):
        return "<br> ".join([g.name for g in self.groups.order_by('name')])
    groups_show.allow_tags = True
    groups_show.short_description = _("groups")

    inlines = [ProfileInline]
    list_display = ('username', 'email', 'first_name', 'last_name',
        groups_show, 'is_staff', 'is_active')
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups',)
    save_on_top = True
    search_fields = ('username', 'first_name', 'last_name',
        'email', 'profile__nick')


admin.site.unregister(User)
admin.site.register(User, ProfileAdmin)
