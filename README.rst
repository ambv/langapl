===============
lukasz.langa.pl
===============

Back to basics. The minimum required to publish anything online.

Authors
=======

Glued together by `Łukasz Langa <mailto:lukasz@langa.pl>`_.
